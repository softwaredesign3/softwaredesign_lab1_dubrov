﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo_Accounting
{
    public class Program
    {
        static void Main(string[] args)
        {

            Lion lion = new Lion { Kindanimal = "Lion", Age = "5 years", };
            Parrot parrot = new Parrot { Kindanimal = "Parrot", Age = "3 years" };
            Bear bear = new Bear { Kindanimal = "Bear", Age = "10 years" };
            Lama lama = new Lama { Kindanimal = "Lama", Age = "8 years" };
            Chimpanzee chimpanzee = new Chimpanzee { Kindanimal = "Chimpanzee", Age = "34 years" };
            Groundhog groundhog = new Groundhog { Kindanimal = "Groundhog", Age = "2 years" };
            Zebra zebra = new Zebra { Kindanimal = "Zebra", Age = "13 years" };
            Giraffe giraffe = new Giraffe { Kindanimal = "Giraffe", Age = "7 years" };


            Cage lionCage = new Cage { Size = 3000, Type = "Savannah" };
            lionCage.AddAnimal(lion);
            lion.Cage = lionCage;


            Cage parrotCage = new Cage { Size = 700, Type = "Tropic" };
            parrotCage.AddAnimal(parrot);
            parrot.Cage = parrotCage;

            Cage bearCage = new Cage { Size = 2500, Type = "Forest" };
            bearCage.AddAnimal(bear);
            bear.Cage = bearCage;

            Cage lamaCage = new Cage { Size = 850, Type = "Sub-tropic" };
            lamaCage.AddAnimal(lama);
            lama.Cage = lamaCage;

            Cage chimpanzeeCage = new Cage { Size = 2000, Type = "Tropic" };
            chimpanzeeCage.AddAnimal(chimpanzee);
            chimpanzee.Cage = chimpanzeeCage;

            Cage groundhogCage = new Cage { Size = 700, Type = "Tropic" };
            groundhogCage.AddAnimal(groundhog);
            groundhog.Cage = groundhogCage;

            Cage zebraCage = new Cage { Size = 3000, Type = "Savannah" };
            zebraCage.AddAnimal(zebra);
            zebra.Cage = zebraCage;

            Cage giraffeCage = new Cage { Size = 700, Type = "Savannah" };
            giraffeCage.AddAnimal(giraffe);
            giraffe.Cage = giraffeCage;

            ZooKeeper zooKeeper = new ZooKeeper { Name = "David", Surname = "Stilinsky", Age = 37, Salary = "10000 UAH", Experience = "3 years" };
            ZooKeeper zooKeeper2 = new ZooKeeper { Name = "Mike", Surname = "Galaxy", Age = 28, Salary = "8000 UAH", Experience = "2 years" };
            ZooKeeper zooKeeper3 = new ZooKeeper { Name = "Stan", Surname = "Milford", Age = 37, Salary = "7500 UAH", Experience = "1 year" };
            ZooKeeper zooKeeper4 = new ZooKeeper { Name = "Rikki", Surname = "Crizly", Age = 48, Salary = "15000 UAH", Experience = "8 years" };
            ZooKeeper zooKeeper5 = new ZooKeeper { Name = "Bob", Surname = "Parkinson", Age = 43, Salary = "15000 UAH", Experience = "8 years" };

            Food meat = new Food { Name = "Meat", Quantity = "130 pcs." };
            Food fruits = new Food { Name = "Fruits", Quantity = "258 pcs." };
            Food fish = new Food { Name = "Fish", Quantity = "210 pcs." };
            Food herbage = new Food { Name = "Herbage", Quantity = "102 pcs." };
            Food leaves = new Food { Name = "Leaves", Quantity = "150 pcs." };

            Inventory inventory = new Inventory();
            Console.WriteLine("Animals ZOO:");
            Console.WriteLine();
            inventory.DisplayAnimalInfo(lionCage.Animals);
            Console.WriteLine();
            inventory.DisplayAnimalInfo(parrotCage.Animals);
            Console.WriteLine();
            inventory.DisplayAnimalInfo(bearCage.Animals);
            Console.WriteLine();
            inventory.DisplayAnimalInfo(lamaCage.Animals);
            Console.WriteLine();
            inventory.DisplayAnimalInfo(chimpanzeeCage.Animals);
            Console.WriteLine();
            inventory.DisplayAnimalInfo(groundhogCage.Animals);
            Console.WriteLine();
            inventory.DisplayAnimalInfo(zebraCage.Animals);
            Console.WriteLine();
            inventory.DisplayAnimalInfo(giraffeCage.Animals);
            Console.WriteLine();
            Console.WriteLine("Zoo Keepers:");
            Console.WriteLine();
            inventory.DisplayEmployeeInfo(new List<ZooKeeper> { zooKeeper });
            Console.WriteLine();
            inventory.DisplayEmployeeInfo(new List<ZooKeeper> { zooKeeper2 });
            Console.WriteLine();
            inventory.DisplayEmployeeInfo(new List<ZooKeeper> { zooKeeper3 });
            Console.WriteLine();
            inventory.DisplayEmployeeInfo(new List<ZooKeeper> { zooKeeper4 });
            Console.WriteLine();
            inventory.DisplayEmployeeInfo(new List<ZooKeeper> { zooKeeper5 });
            Console.WriteLine();
            List<Food> foods = new List<Food> { meat, fruits , fish, herbage, leaves };
            Console.WriteLine("Food Inventory:");
            Console.WriteLine();
            inventory.DisplayFoodInfo(foods);
        }
    }
}
