﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo_Accounting
{
    public class Cage
    {
        public int Size { get; set; }
        public string Type { get; set; }
        public List<Animals> Animals { get; set; }

        public Cage()
        {
            Animals = new List<Animals>();
        }

        public void AddAnimal(Animals animals)
        {
            Animals.Add(animals);
        }

        public void RemoveAnimal(Animals animals)
        {
            Animals.Remove(animals);
        }

    }
}
