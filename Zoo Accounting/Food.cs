﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo_Accounting
{
    public class Food
    {
        public string Name { get; set; }
        public string Quantity { get; set; }
    }
}
