﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo_Accounting
{
    public class Inventory
    {
        public void DisplayAnimalInfo(List<Animals> animals)
        {
            foreach (var animal in animals)
            {
                Console.WriteLine($"Kindanimal: {animal.Kindanimal}, Age: {animal.Age}");
                animal.SpeakingSound();
                if (animal.Cage != null)
                {
                    Console.WriteLine($"Cage: Size - {animal.Cage.Size}, Type - {animal.Cage.Type}");
                }
            }
        }

        public void DisplayEmployeeInfo(List<ZooKeeper> zooKeepers)
        {
            foreach (var keeper in zooKeepers)
            {
                Console.WriteLine($"Name: {keeper.Name}, Surname: {keeper.Surname}, Age: {keeper.Age}, Salary: {keeper.Salary}, Experience: {keeper.Experience} ");
            }
        }
        public void DisplayFoodInfo(List<Food> foods)
        {
            foreach (var food in foods)
            {
                Console.WriteLine($"Food: {food.Name}, Quantity: {food.Quantity}");
            }
        }
    }
}


