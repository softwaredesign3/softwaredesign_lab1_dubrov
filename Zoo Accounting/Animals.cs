﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo_Accounting
{
    public abstract class Animals
    {
        public string Kindanimal { get; set; }
        public string Age { get; set; }
        public Cage Cage { get; set; }
        public abstract void SpeakingSound();
    }
}
