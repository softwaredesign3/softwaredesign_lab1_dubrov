## ZooAccounting Diagram
![ZooAccounting Diagram](https://gitlab.com/softwaredesign3/softwaredesign_lab1_dubrov/-/raw/main/ZooAccounting.drawio.png?ref_type=heads)
## List of principlces used in Lab01
* DRY (Don't Repeat Yourself):

This principle is followed in the code by avoiding duplication. For example, the method DisplayAnimalInfo, DisplayEmployeeInfo, and DisplayFoodInfo in the Inventory class are used to display information about animals, employees, and food respectively.
* KISS (Keep It Simple, Stupid):

The code maintains a simple and understandable structure. For instance, adding and removing animals from a cage in the Cage class is implemented with simple methods like AddAnimal and RemoveAnimal.
* SOLID:

> S - Single Responsibility Principle: Each class has a single responsibility. For instance, Animals defines the type of animal, Cage manages the cage, and ZooKeeper manages the zookeeper's data. (Animals.cs, Cage.cs, ZooKeeper.cs)
> O - Open/Closed Principle: The code can be extended without modifying existing code.
> L - Liskov Substitution Principle: Subclasses (Parrot, Lion, etc.) can be used interchangeably with the base class Animals without changing the program's behavior.
> I - Interface Segregation Principle: Interfaces are not overly large, each class implements only necessary methods.
> D - Dependency Inversion Principle: Classes depend on abstractions (e.g., Animals depends on Cage abstraction) rather than concrete implementations.
* YAGNI (You Aren't Gonna Need It):

The code doesn't add unnecessary functionality or structures that aren't currently needed.
*Composition Over Inheritance:

Composition is preferred over inheritance for relationships between classes, for example, the relationship between Animals and Cage.
* Program to Interfaces not Implementations:

The code uses abstractions (e.g., Animals, Cage, ZooKeeper, Food) for object interactions.
* Fail Fast:

There are no situations in the code where errors could go unnoticed. For example, adding an animal that already exists in the cage.
* The DisplayAnimalInfo method:

This method displays information about the animals in the list. It simply goes through the list of animals and displays their information and the sound they make.
```
public void DisplayAnimalInfo(List<Animals> animals)
{
    foreach (var animal in animals)
    {
        Console.WriteLine($"Kindanimal: {animal.Kindanimal}, Age: {animal.Age}");
        animal.SpeakingSound();
        if (animal.Cage != null)
        {
            Console.WriteLine($"Cage: Size - {animal.Cage.Size}, Type - {animal.Cage.Type}");
        }
    }
}

```
* The DisplayEmployeeInfo method:

This method displays information about the zoo employees. It also simply goes through the list of zookeepers and displays their information.
```
public void DisplayEmployeeInfo(List<ZooKeeper> zooKeepers)
{
    foreach (var keeper in zooKeepers)
    {
        Console.WriteLine($"Name: {keeper.Name}, Surname: {keeper.Surname}, Age: {keeper.Age}, Salary: {keeper.Salary}, Experience: {keeper.Experience} ");
    }
}
```
*  The DisplayFoodInfo method:

This method displays information about the food that is available in the zoo. It also simply goes through the list of food and displays the name and quantity.
```
public void DisplayFoodInfo(List<Food> foods)
{
    foreach (var food in foods)
    {
        Console.WriteLine($"Food: {food.Name}, Quantity: {food.Quantity}");
    }
}
```
* These methods demonstrate the DRY principle, as they avoided duplicating code to output information about animals, pet keepers, and food, and ensure that the output logic is reused.

